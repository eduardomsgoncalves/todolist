package com.example.lista

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView

class ListaAdapter(val afazerDao : AfazerDao)
    : RecyclerView.Adapter<ListaAdapter.AFazerHolder>() {

    companion object {
          lateinit var afazer: Afazer;
          lateinit var afazerDao: AfazerDao;
    }

    lateinit var db: AppDatabase
    val dados: MutableList<Afazer>
    init {
        dados = afazerDao.all as MutableList<Afazer>
        ListaAdapter.afazerDao = afazerDao;
    }

    class AFazerHolder(v: View, val dados: MutableList<Afazer>)
        : RecyclerView.ViewHolder(v) {

        val title: TextView
        val description: TextView
        val remove: ImageButton

        init {
            title = v.findViewById(R.id.editTitle)
            description = v.findViewById(R.id.editDescription)
            remove = v.findViewById(R.id.remove)

            title.setOnClickListener(::detalhes)
            description.setOnClickListener(::detalhes)
        }

        fun detalhes(v: View) {
            afazer = dados[adapterPosition]
            v.findNavController().navigate(R.id.lista_para_detalhes)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AFazerHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_lista, parent, false)

        return AFazerHolder(view, dados)
    }

    override fun onBindViewHolder(holder: AFazerHolder, position: Int) {
        holder.title.text = dados[position].title
        holder.description.text = dados[position].description

        holder.remove.setOnClickListener {
            remove(position)
        }
    }

    override fun getItemCount() = dados.size

    fun remove(position: Int) {
        val afazer = dados[position]

        afazerDao.delete(afazer)
        dados.removeAt(position)

        notifyItemRemoved(position)
        notifyItemRangeChanged(position, itemCount - position)
    }

    fun adicionar(title: String) {
        val afazer = Afazer(title, "Descrição da Tarefa")

        afazerDao.insert(afazer);
        dados.add(afazer)
        notifyItemInserted(0)
        notifyItemRangeChanged(0, itemCount)
    }

}