package com.example.lista

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Afazer(var title: String, var description: String) {

    @PrimaryKey(autoGenerate = true)
    var id : Long = 0

}