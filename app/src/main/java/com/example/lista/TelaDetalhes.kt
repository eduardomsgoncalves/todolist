package com.example.lista

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView

class TelaDetalhes : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tela_detalhes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val editTitle: TextView = view.findViewById(R.id.editTitle)
        editTitle.text = ListaAdapter.afazer.title

        val editDescription: TextView = view.findViewById(R.id.editDescription)
        editDescription.text = ListaAdapter.afazer.description

        val btnSave: Button = view.findViewById(R.id.btnSave)
        btnSave.setOnClickListener {
            ListaAdapter.afazer.title = editTitle.text.toString()
            ListaAdapter.afazer.description =  editDescription.text.toString()

            ListaAdapter.afazerDao.update(ListaAdapter.afazer)
             activity?.onBackPressed();
        }
        super.onViewCreated(view, savedInstanceState)

    }

}